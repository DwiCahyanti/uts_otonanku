import 'package:flutter/material.dart';
import 'landingpage.dart';

void main() => runApp(
    MaterialApp(
      theme: ThemeData(primaryColor: Color(0xFF67CDDC),),
      debugShowCheckedModeBanner: false,
      home:  landingpage(),
    )
);