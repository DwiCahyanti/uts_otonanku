import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:otonanku/inputdata.dart';
import 'package:otonanku/landingpage.dart';

class hasil extends StatelessWidget {

  final DateTime tanggal_lahir;

  const hasil({Key key, this.tanggal_lahir}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    String hari;
    String pancawara;
    String wuku;
    DateTime otonan1;
    DateTime otonan2;
    int hitunghari;


    var tl = tanggal_lahir;
    var patokan = DateTime(2020, 11, 25);
    final selisih = patokan.difference(tl).inDays;

    var lamahidup = DateTime.now().difference(tl).inDays;
    var oton = lamahidup%210;

    if(oton == 0)
      otonan1=DateTime.now();
    else
      otonan1=DateTime.now().subtract(new Duration(days: oton));

    if(oton == 0)
    otonan2=DateTime.now().add(Duration (days: 210));
    else
    otonan2=DateTime.now().add(Duration (days: 210-oton));

    DateTime tgl_oton1 = otonan1;
    String tgl_otonan1 = DateFormat('dd-MM-yyyy').format(tgl_oton1);
    DateTime tgl_oton2 = otonan2;
    String tgl_otonan2 = DateFormat('dd-MM-yyyy').format(tgl_oton2);

    hitunghari = selisih%7;
    if (hitunghari == 0)
      hari = 'Rabu';
    else if (hitunghari == 1)
      hari = 'Selasa';
    else if (hitunghari == 2)
      hari = 'Senin';
    else if (hitunghari == 3)
      hari = 'Minggu';
    else if (hitunghari == 4)
      hari = 'Sabtu';
    else if (hitunghari == 5)
      hari = 'Jumat';
    else
      hari = 'Kamis';

    var hitungpw = selisih%5;
    if (hitungpw == 0)
      pancawara = 'Kliwon';
    else if (hitungpw == 1)
      pancawara = 'Wage';
    else if (hitungpw == 2)
      pancawara = 'Pon';
    else if (hitungpw == 3)
      pancawara = 'Pahing';
    else if (hitungpw == 4)
      pancawara = 'Umanis';

    var hitungwk = (selisih%210)/7;
    var hitungwuku = hitungwk.round();
    if (hitungwuku == 0)
      wuku = 'Matal';
    else if (hitungwuku == 1)
      wuku = 'Medangkungan';
    else if (hitungwuku == 2)
      wuku = 'Tambir';
    else if (hitungwuku == 3)
      wuku = 'Merakih';
    else if (hitungwuku == 4)
      wuku = 'Krulut';
    else if (hitungwuku == 5)
      wuku = 'Pahang';
    else if (hitungwuku == 6)
      wuku = 'Pujut';
    else if (hitungwuku == 7)
      wuku = 'Medangsia';
    else if (hitungwuku == 8)
      wuku = 'Langkir';
    else if (hitungwuku == 9)
      wuku = 'Kuningan';
    else if (hitungwuku == 10)
      wuku = 'Dungulan';
    else if (hitungwuku == 11)
      wuku = 'Sungsang';
    else if (hitungwuku == 12)
      wuku = 'Julungwangi';
    else if (hitungwuku == 13)
      wuku = 'Warigadian';
    else if (hitungwuku == 14)
      wuku = 'Wariga';
    else if (hitungwuku == 15)
      wuku = 'Gumreg';
    else if (hitungwuku == 16)
      wuku = 'Tolu';
    else if (hitungwuku == 17)
      wuku = 'Kulantir';
    else if (hitungwuku == 18)
      wuku = 'Ukir';
    else if (hitungwuku == 19)
      wuku = 'Landep';
    else if (hitungwuku == 20)
      wuku = 'Sinta';
    else if (hitungwuku == 21)
      wuku = 'Watugunung';
    else if (hitungwuku == 22)
      wuku = 'Dukut';
    else if (hitungwuku == 23)
      wuku = 'Klau';
    else if (hitungwuku == 24)
      wuku = 'Wayang';
    else if (hitungwuku == 25)
      wuku = 'Ugu';
    else if (hitungwuku == 26)
      wuku = 'Bala';
    else if (hitungwuku == 27)
      wuku = 'Prangbakat';
    else if (hitungwuku == 28)
      wuku = 'Menail';
    else if (hitungwuku == 29)
      wuku = 'Uye';
    else
      wuku = 'Matal';


    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 100),
                      child: Image.asset(
                        'images/logo.png',
                        height: 200,
                      )),
                  Container(
                      margin: EdgeInsets.only(top: 30, left: 60,right: 60),
                      child: Text('Berikut data yang kami dapat dari tanggal lahir kamu:',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ))),
                  Container(
                      margin: EdgeInsets.only(top: 60),
                      child: Text('Saptawara: $hari',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text('Pancawara: $pancawara',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text('Wuku: $wuku',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.w600,
                          ))),
                  Container(
                      margin: EdgeInsets.only(top: 60),
                      child: Text('Otonan terdekatmu:',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                          ))),
                  Container(
                      margin: EdgeInsets.only(top: 10),
                      child: Text(((){
                        if(tgl_oton1.isBefore(DateTime.now())){
                            return '$tgl_otonan2';}
                        return '$tgl_otonan1';
                      })(),
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                          ),
                      )),
                  Container(
                    margin: EdgeInsets.only(left: 90, right: 90, top: 100),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => inputdata()),
                        );
                      },
                      padding: EdgeInsets.only(top: 15, bottom: 15),
                      color: Colors.red,
                      child: Text(
                        'Selesai',
                        style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ),
                  ),
                ])));
  }
}
