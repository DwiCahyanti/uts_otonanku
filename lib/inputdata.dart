import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'hasil.dart';
import 'package:google_fonts/google_fonts.dart';

class inputdata extends StatefulWidget {
  @override
  _inputdataState createState() => _inputdataState();
}

class _inputdataState extends State<inputdata> {

  DateTime tanggal_lahir;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 250),
                  child: Image.asset(
                    'images/kalender.png',
                    height: 150,
                  )),
                  Container(
                      margin: EdgeInsets.only(top:30 ,left: 60,right: 60),
                      child: Text('"Kami membutuhkan tanggal lahirmu untuk menemukan kapan otonanmu"',
                          textAlign: TextAlign.center,
                          style: GoogleFonts.poppins(
                            fontSize: 14,
                            fontWeight: FontWeight.bold,
                          ))),
              Container(
                margin: EdgeInsets.only(top: 200, left: 90, right: 90),
                child: RaisedButton(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0),
                  ),
                  onPressed: () {
                    DatePicker.showDatePicker(context,
                        showTitleActions: true,
                        minTime: DateTime(1945, 1, 1),
                        maxTime: DateTime(2045, 12, 31), onChanged: (date) {
                      tanggal_lahir = date;
                    }, onConfirm: (date) {
                      tanggal_lahir = date;
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => hasil(
                                  tanggal_lahir: tanggal_lahir,
                                )),
                      );
                    }, currentTime: DateTime.now(), locale: LocaleType.id);
                  },
                  padding: EdgeInsets.only(top: 15, bottom: 15),
                  color: Colors.lightBlue,
                  child: Text(
                    'Masukkan tanggal lahir',
                    style: GoogleFonts.poppins(
                        fontSize: 12,
                        fontWeight: FontWeight.w500,
                        color: Colors.white),
                  ),
                ),
              ),
            ])));
  }
}
