import 'package:flutter/material.dart';
import 'package:otonanku/inputdata.dart';
import 'package:google_fonts/google_fonts.dart';

class landingpage extends StatefulWidget {
  @override
  _landingpageState createState() => _landingpageState();
}

class _landingpageState extends State<landingpage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: SingleChildScrollView(
            child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
              Container(
                  margin: EdgeInsets.only(top: 250),
                  child: Image.asset(
                    'images/logo.png',
                    height: 200,
                  )),
              Container(
                  margin: EdgeInsets.only(top: 10),
                  child: Text('OTONANKU',
                      textAlign: TextAlign.center,
                      style: GoogleFonts.poppins(
                        fontSize: 24,
                        fontWeight: FontWeight.bold,
                      ))),
                  Container(
                    margin: EdgeInsets.only(left: 90, right: 90, top: 200),
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                      onPressed: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => inputdata()),
                        );
                      },
                      padding: EdgeInsets.only(top: 15, bottom: 15),
                      color: Colors.green,
                      child: Text(
                        'Temukan otonanku',
                        style: GoogleFonts.poppins(
                            fontSize: 12,
                            fontWeight: FontWeight.w500,
                            color: Colors.white),
                      ),
                    ),
                  ),
            ])));
  }
}
